import { Component } from '@angular/core';

@Component({
  selector: 'movies-block',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'home-work';
}
