import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})

export class Movie {
  @Input() name = 'Thor';
  @Input() poster = 'https://www.iconspng.com/images/primary-unknown/primary-unknown.jpg';
  @Input() realizeDate = '2002 year';
}
